
// Activity:
// DONE
// 1. In the S15 folder, create an activity folder, an index.html file inside of it and link the index.js file.
// DONE
// 2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
console.log("Hello World")
// 3. Prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
// while (!isNaN(parseInt(prompt("Input the first number: "))))
// let num1 = prompt("Input the first number: ")
// let num2 = prompt("Input the second number: ")


let num1 = parseFloat(prompt("Input the first number: "))
let num2 = parseFloat(prompt("Input the second number: "))
let valid = false


function validateRealNumber(realNumber) { 

    let rx = /^\d+(?:\.\d{1,2})?$/ 

    if(rx.test(realNumber)) { 
       return true; 
    }
    else { 
       return false; 
    } 
}

do {

	if (validateRealNumber(num1) && validateRealNumber(num2)) {
		valid = true
	} else {
		num1 = parseFloat(prompt("Input a valid number for the first number: "))
		num2 = parseFloat(prompt("Input a valid number for the second number: "))
	}
} while (!valid)

let total = num1 + num2

console.log(num1, num2)
console.log(total)


// let sum = (n1, n2) => (isNaN(n1) && isNaN(num2)) ? a
// - If the total of the two numbers is less than 10, add the numbers
let sum = (n1, n2) => n1 + n2

// - If the total of the two numbers is 10 - 19, subtract the numbers
let diff = (n1, n2) => n1 - n2

// - If the total of the two numbers is 20 - 29 multiply the numbers
let product = (n1, n2) => n1 * n2

// - If the total of the two numbers is greater than or equal to 30, divide the numbers
let div = (n1, n2) => n1 / n2

// 4. Use an alert for the total of 10 or greater and a console warning for the total of 9 or less.
// STRETCH GOALS:

switch (true) {
	case total < 10: console.warn(sum(num1, num2)); break
	case total >= 10 && total <= 19: console.log(diff(num1, num2)); break
	case total >= 20 && total <= 29: console.log(product(num1,num2)); break
	case total >= 30: console.log(div(num1, num2)); break
	default: console.log(total)
}


// 5. Prompt the user for their name and age and print out different alert messages based on the user input:
let name = prompt("Enter your name: ")
let age = parseInt(prompt("Enter your age: "))

// -  If the name OR age is blank/null, print the message are you a time traveler?
console.log( ((name === '' && name === null) || (age === '' && age === null)) ? 
	console.log("Are you a time traveler?") :
// -  If the name AND age is not blank, print the message with the user’s name and age.	
	console.log(name + " " + " " + age + " Are you a time traveler?")
)

// 6. Create a function named isLegalAge which will check if the user's input from the previous prompt is of legal age:
let isLegalAge = num => num >= 18 ? 
// - 18 or greater, print an alert message saying You are of legal age.
alert("You are of legal age.") : 
// - 17 or less, print an alert message saying You are not allowed here.
alert("You are not allowed here.")
// Invoke function
isLegalAge(age)


// 8. Create a switch case statement that will check if the user's age input is within a certain set of expected input:
switch(age) {
	// - 18 - print the message You are now allowed to party.
	case 18: console.log("You are now allowed to party."); break;
	// - 21 - print the message You are now part of the adult society.
	case 21: console.log("You are now part of the adult society"); break;
	// - 65 - print the message We thank you for your contribution to society.
	case 65: console.log("We thank you for your contribution to society."); break;
	// - Any other value - print the message Are you sure you're not an alien?
	default: console.log("Are you sure you're not an alien?")
}

// 8. Create a try catch finally statement to force an error, 
// print the error message as a warning and use the function isLegalAge to print alert another message.
try {
	alerat("forced error")
}
catch(err) {
	console.warn(err.message)
}
finally {
	isLegalAge(age)
}
// 9. Create a git repository named S15.
// 10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 11. Add the link in Boodle.


// Explaination of expression

// /^\d+(?:\.\d{1,2})?$/             

// \d       match a digit...   
// +        one or more times    
// (        begin group...     
// ?:       but do not capture anything     
// \.       match literal dot     
// \d       match a digit...     
// {1,2}    one or two times    
// )        end group   
// ?        make the entire group optional 
// Cheers